package com.perfaware.kafka.consumer;

import java.util.concurrent.CountDownLatch;

import javax.jms.JMSException;
import javax.jms.Queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.transaction.KafkaTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class Receiver {
	@Autowired 
	JmsTemplate jmsTemplate;
	
	@Autowired
	Queue queue;
	
	/*
	 * @Autowired KafkaTransactionManager kafkaTransactionManager;
	 */

   public static final String INPUT_TEST_TOPIC = "thirdtopic";
  private static final Logger LOGGER =
      LoggerFactory.getLogger(Receiver.class);

  private CountDownLatch latch = new CountDownLatch(1);

  public CountDownLatch getLatch() {
    return latch;
  }

	/* @Transactional */
  @KafkaListener(topics = INPUT_TEST_TOPIC)
  public void receive(String payload ) {
	  
 
    latch.countDown();
   
    LOGGER.info("Received Kafka record from {}: {}", INPUT_TEST_TOPIC, payload);
    jmsTemplate.setDeliveryPersistent(true);
	jmsTemplate.convertAndSend(queue,payload);

	
	 LOGGER.info("forwarded payload='{}'", payload );
  }  
  


	  
  }

