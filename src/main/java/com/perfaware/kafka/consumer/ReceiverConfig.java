package com.perfaware.kafka.consumer;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.jms.MessageListener;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.requests.IsolationLevel;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.kafka.ConcurrentKafkaListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.config.KafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ConcurrentMessageListenerContainer;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.transaction.KafkaTransactionManager;
import org.springframework.transaction.annotation.Isolation;

@Configuration
@EnableKafka
public class ReceiverConfig {

  @Value("${kafka.bootstrap-servers}")
  private String bootstrapServers;

  @Bean
  public Map<String, Object> consumerConfigs() {
    Map<String, Object> props = new HashMap<>();
    props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
        bootstrapServers);
    props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
        StringDeserializer.class);
    props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
        StringDeserializer.class);
    props.put(ConsumerConfig.GROUP_ID_CONFIG, "reveiver");
    props.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed" );
    props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
    props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,"latest");
  

    return props;
  }

	
	  @Bean public ConsumerFactory<?, ?> consumerFactory() { return new
	  DefaultKafkaConsumerFactory<>(consumerConfigs()); }
	 

 /* @Bean
  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory =
        new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    

    return factory;
  }*/
  @Bean
  public KafkaListenerContainerFactory<ConcurrentMessageListenerContainer<String, String>> kafkaListenerContainerFactory(
     
      ConsumerFactory<Object, Object> consumerFactory,
      KafkaTransactionManager<Object, Object> kafkaTransactionManager) {
    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
  
    factory.setConsumerFactory((ConsumerFactory<String, String>) consumerFactory());
    factory.getContainerProperties().setTransactionManager(kafkaTransactionManager);
    return factory;
  }

	/*
	 * @Bean public ConcurrentKafkaListenerContainerFactory<?, ?>
	 * kafkaListenerContainerFactory(
	 * ConcurrentKafkaListenerContainerFactoryConfigurer configurer,
	 * ConsumerFactory<Object, Object> kafkaConsumerFactory,
	 * KafkaTransactionManager<Object, Object> kafkaTransactionManager) {
	 * ConcurrentKafkaListenerContainerFactory<Object, Object> factory = new
	 * ConcurrentKafkaListenerContainerFactory<>(); configurer.configure(factory,
	 * kafkaConsumerFactory);
	 * factory.getContainerProperties().setTransactionManager(
	 * kafkaTransactionManager); return factory; }
	 */
 
	/*
	 * @Bean public Receiver receiver() {
	 * 
	 * return new Receiver(); }
	 */
  }
  

